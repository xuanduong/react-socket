# stage1 - build react app first
FROM public.ecr.aws/docker/library/node:18-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json yarn.lock /app/

RUN yarn install

COPY . .

RUN yarn build

# stage 2 - build the final image and copy the react build files
FROM public.ecr.aws/nginx/nginx:1.25-alpine

ENV USER=executor
ENV GROUP=executor

# config env flexible way
COPY nginx/env.sh /docker-entrypoint.d/env.sh

# copy artifacts from build stage
COPY --from=build /app/build /usr/share/nginx/html

# config nginx configuration
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf

RUN chmod +x /docker-entrypoint.d/env.sh

RUN addgroup -g 1098 $GROUP \
    && adduser -D -u 1098 $USER -G $GROUP \
    && chown -R $USER:$GROUP /var/cache/nginx \
    && chown -R $USER:$GROUP /var/log/nginx \
    && mkdir -p /var/lib/nginx && chown -R $USER:$GROUP /var/lib/nginx \
    && chown -R $USER:$GROUP /etc/nginx \
    && chown -R $USER:$GROUP /docker-entrypoint.d/env.sh \
    && chown -R $USER:$GROUP /usr/share/nginx/html

RUN touch /run/nginx.pid \
 && chown -R $USER:$GROUP /run/nginx.pid 

RUN sed -i 's/user  nginx;/#user  nginx;/g' /etc/nginx/nginx.conf

RUN chmod -R 777 /etc/nginx/conf.d

USER $USER

EXPOSE 8080 8443
CMD ["nginx", "-g", "daemon off;"]
